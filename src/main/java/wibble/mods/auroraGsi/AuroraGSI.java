package wibble.mods.auroraGsi;

import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent.OnConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = AuroraGSI.MODID, useMetadata = true, clientSideOnly = true)
public class AuroraGSI {

    public static final String MODID = "auroragsi";

    private static Logger logger;

    /**
     * Grab a reference to the logger, and show a warning on servers that the mod isn't needed.
     */
    @EventHandler
    public void preInit(FMLPreInitializationEvent evt) {
        logger = evt.getModLog();
        MinecraftForge.EVENT_BUS.register(this); // Required to listen for config event
    }

    /**
     * Start a timer that will send a request to the Aurora HTTP listening server containing the game data.
     */
    @EventHandler
    public void init(FMLInitializationEvent evt) {
        ConfigManager.sync(MODID, Config.Type.INSTANCE); // Initial settings sync
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(new SendGameState(), 0, AuroraGSIConfig.UpdateRate, TimeUnit.MILLISECONDS);
    }

    // Listen for config changes and save them (using sync).
    @SubscribeEvent
    public void onConfigChangedEvent(OnConfigChangedEvent event) {
        if (event.getModID().equals(MODID))
            ConfigManager.sync(MODID, Config.Type.INSTANCE);
    }

    /** Settings store class. */
    @Config(modid = MODID, type = Config.Type.INSTANCE)
    public static class AuroraGSIConfig {
        @Config.Name("Aurora Port")
        @Config.Comment({"This should probably not need to be changed!", "The target port that the GSI updates should be POSTed to. Should match the port that Aurora is listening on."})
        @Config.RangeInt(min = 1, max = 65535)
        @Config.RequiresMcRestart
        public static int AuroraPort = 9088;

        @Config.Name("Update Rate (ms)")
        @Config.Comment({"The rate at which the Aurora GSI mod sends data to Aurora application.", "The number of updates per second can be calculated by taking 1000 and dividing it by the number here. E.G. update rate of 100 means 10 updates per second."})
        @Config.RangeInt(min = 50, max = 1500)
        @Config.RequiresMcRestart
        public static int UpdateRate = 100;
    }
}